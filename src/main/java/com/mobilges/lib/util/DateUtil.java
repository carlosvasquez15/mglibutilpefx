package com.mobilges.lib.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author as
 */
public class DateUtil{

    public static final long limiteSuperiorTransac = Integer.MAX_VALUE;

    public static XMLGregorianCalendar convertDateToXmlGregorianCalendar(Date fecha){
        GregorianCalendar gcal = new GregorianCalendar();
        gcal.setTime(fecha);
        XMLGregorianCalendar xmlgcal = null;
        try {
            xmlgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(DateUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return xmlgcal;
    }

    public static java.util.GregorianCalendar getGregFromDate(java.util.Date fecha) {
        GregorianCalendar gcal = new GregorianCalendar();
        gcal.setTime(fecha);
        return gcal;
    }

    public static java.util.Date getDateFromGreg(java.util.GregorianCalendar gcal){
        java.util.Date date = new java.util.Date();
        date = gcal.getTime();
        return date;
    }

    public static java.util.Date getToday(){
        return new Date();
    }

    public static java.util.Date getLastDayOfMonth(int mes, int anio) {
        GregorianCalendar fechaGC = new GregorianCalendar(anio, mes + 1, 1);
        fechaGC.add(Calendar.DAY_OF_MONTH, -1);
        java.util.Date fechaUltimoDia = new Date(fechaGC.getTimeInMillis());
        return fechaUltimoDia;
    }

    public static java.util.Date getLasDayOfYear(int anio) {
        GregorianCalendar fechaGC = new GregorianCalendar( anio, 11, 31);
        java.util.Date ultimoDiaAnio = new Date(fechaGC.getTimeInMillis());
        return ultimoDiaAnio;
    }

    public static java.util.Date getDateForMonday(GregorianCalendar fechaGC) {
        // Monday = 2
        int diasResta = fechaGC.get(Calendar.DAY_OF_WEEK);
        fechaGC.add(Calendar.DATE, -(diasResta - 1));
        return new Date(fechaGC.getTimeInMillis());
    }

    public static java.util.Date getDateForSunday(GregorianCalendar fechaGC) {
        int diasSuma = 7 - fechaGC.get(Calendar.DAY_OF_WEEK);
        fechaGC.add(Calendar.DATE, diasSuma);
        return new Date(fechaGC.getTimeInMillis());
    }

    public static Date getYear3000() {
        return new java.util.Date(new GregorianCalendar(3000, Calendar.JANUARY, 1).getTimeInMillis());
    }

    public static java.util.Date getYear1900() {
        return new Date(0);
    }

    public static java.util.Date get15PrevDays() {
        return new Date(new Date().getTime() - 15 * 24 * 3600 * 1000);
    }

    public static java.util.Date get15NextDays() {
        return new Date(new Date().getTime() + 15 * 24 * 3600 * 1000);
    }


    public static java.util.Date getNPrevDays(int nDays) {
        return new Date(new Date().getTime() - (long)nDays * 24 * 3600 * 1000);
    }

    public static java.util.Date getNNextDays(int nDays) {
        return new Date(new Date().getTime() + (long)nDays * 24 * 3600 * 1000);
    }
    
    public static java.util.Date getNPrevDays( Date date, int nDays) {
        return new Date( date.getTime() - (long)nDays * 24 * 3600 * 1000);
    }

    public static java.util.Date getNNextDays( Date date, int nDays) {
        return new Date( date.getTime() + (long)nDays * 24 * 3600 * 1000);
    }

    public static java.util.Date getDate00Hr(Date fecha) {
        GregorianCalendar fechaGc = new GregorianCalendar();
        fechaGc.setTime(fecha);
        GregorianCalendar fecha00Gc = new GregorianCalendar(fechaGc.get(Calendar.YEAR), fechaGc.get(Calendar.MONTH), fechaGc.get(Calendar.DAY_OF_MONTH));
        return fecha00Gc.getTime();
    }

    // devuelve fecha en formato dd/mm/yyyy
    public static String getDateddmmyy(Date fecha) {
        GregorianCalendar fechaGc = getGregFromDate(fecha);
        int mes = fechaGc.get(Calendar.MONTH) + 1;
        String fechaFormato = fechaGc.get(Calendar.DAY_OF_MONTH) + "/" + mes + "/" + fechaGc.get(Calendar.YEAR);
        return fechaFormato;
    }

    public static int getDayOfMonth(Date fecha){
        GregorianCalendar fechaGc = getGregFromDate(fecha);
        return fechaGc.get(Calendar.DAY_OF_MONTH);
    }

    public static int getMonthOfYear(Date fecha){
        GregorianCalendar fechaGc = getGregFromDate(fecha);
        int mes = fechaGc.get(Calendar.MONTH) + 1;
        return mes;
    }

    public static int getYearOfDate(Date fecha){
        GregorianCalendar fechaGc = getGregFromDate(fecha);
        return fechaGc.get(Calendar.YEAR);
    }

    /**
     * Forma un String de horas minutos y segundos
     * @param timeInSeconds
     * @return
     */
    public static String calcHMS(Long timeInMiliSeconds) {
        long hours, minutes, seconds;
        long timeInSeconds = timeInMiliSeconds / 1000;
        hours = timeInSeconds / 3600;
        timeInSeconds = timeInSeconds - (hours * 3600);
        minutes = timeInSeconds / 60;
        timeInSeconds = timeInSeconds - (minutes * 60);
        seconds = timeInSeconds;
        return hours + "hr(s) " + minutes + " min(s) " + seconds +" sec(s)";
    }

    public static String calcHMS(long timeInMiliSeconds) {
        return calcHMS(new Long(timeInMiliSeconds));
    }

    /**
     * Suma 23 horas con 59 minutos con 59 segundos, util en pantallas de consulta
     */
    public static Date sumDay(Date originalDate, int days){
        if(originalDate!=null){
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(originalDate);
            calendar.add(Calendar.DATE, days);
            calendar.add(Calendar.SECOND, -1);
            return calendar.getTime();
        }else{
            return null;
        }
    }

    /**
     * Elimina los datos respecto al tiempo conservando la fecha
     */
    public static Date removeTimeData(Date originalDate){
        Calendar cal = Calendar.getInstance();
        cal.setTime(originalDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0); 
        return cal.getTime();
    }

    /**
     * Obtiene una fecha apartir de un int de días a partir del 1 de enero de 1900
     * */

    public static Date dateFrom1900(int days){
        Calendar cal = Calendar.getInstance();
        cal.set(1900, Calendar.JANUARY, 1);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }
    
    /* el parametro epoch es formato Epoch sin milisegundos   */
    public static Date dateFromEpoch( long epoch ){
        Date fecha = new Date( epoch * 1000L );
        return fecha;
    }
    
    public static String getCurrentTime( ){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm:ss" );
        return sdf.format( cal.getTime() );
    }
    
    public static Date getFechaProg() {
        Calendar c1 = GregorianCalendar.getInstance();
        c1.set(Calendar.HOUR_OF_DAY, 0); //anything 0 - 23
        c1.set(Calendar.MINUTE, 0);
        c1.set(Calendar.SECOND, 0);
        c1.add( Calendar.DATE, 1 );
        
        return c1.getTime();
    }


}
