
package com.mobilges.lib.util;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 *
 * @author victor
 * as - agregar redondeo de un doble.
 */

public class NumberUtil {

    /**
     * Establece una escala a todas las propiedades bigDecimal dentro del objeto
     */
    public static Object redondearBigDecimal(Object pObjeto, int escala){
        Method metodos[] = pObjeto.getClass().getMethods();
        for (int i = 0; i < metodos.length; i++) {
            Method metodo = metodos[i];
            //Si es un metodo get o is lo utilizo con su equivalente set
            if ((metodo.getName().substring(0, 3).equalsIgnoreCase("get") || metodo.getName().substring(0, 2).equalsIgnoreCase("is")) && !metodo.getName().equals("getClass")) {
                String methodNameSet = "";
                if(metodo.getName().substring(0, 3).equalsIgnoreCase("get")){
                    methodNameSet = metodo.getName().replaceFirst("get", "set");
                }else{
                    methodNameSet = methodNameSet.replaceFirst("is", "set");
                }
                try {
                    Method metodoSet = pObjeto.getClass().getMethod(methodNameSet, metodo.getReturnType());
                    if (metodoSet != null) {
                        //Datos numericos
                        //Si es bigDecimal
                        if (metodo.getReturnType().equals(java.math.BigDecimal.class)) {
                            BigDecimal valor = (BigDecimal)metodo.invoke(pObjeto, new Object[0]);
                            metodoSet.invoke(pObjeto, valor.setScale(escala, RoundingMode.HALF_UP));

                        }
                        //En el caso de listados dentro de objetos se utiliza una llamada recursiva
                        if (metodo.getReturnType().equals(java.util.List.class)||metodo.getReturnType().equals(java.util.ArrayList.class)) {
                            List listado = (List)metodo.invoke(pObjeto, new Object[0]);
                            for(Object elementoListado:listado){
                                elementoListado = redondearBigDecimal(elementoListado, escala);
                            }
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
        return pObjeto;
    }

    /**
     * El comportamiento predeterminado son dos decimales
     */
    public static Object redondearBigDecimal(Object pObjeto){
        return redondearBigDecimal(pObjeto, 2);
    }

    public static double redondearDouble(double pDouble, int escala){
        BigDecimal redondeado = (BigDecimal) redondearBigDecimal(new BigDecimal(pDouble), escala);
        return redondeado.doubleValue();
    }
    /*
     * Calcular valor total de precio multiplicado por cantidad ingresado en formato CAJA.UNIDAD
     */
    public static BigDecimal getValorLineaPedidoEnCajasPuntoUnidades(String pCantidadPedida, BigDecimal _precioCaja, long unidadesPorCaja){

        BigDecimal _cantidadPedida = new BigDecimal(pCantidadPedida);
        long cajasPedidas = _cantidadPedida.longValue();
        BigDecimal _cajasPedidas = new BigDecimal(cajasPedidas+"");

        unidadesPorCaja = unidadesPorCaja == 0 ? 1 : unidadesPorCaja;
        BigDecimal _unidadesPorCaja = new BigDecimal(unidadesPorCaja+"").setScale(0, RoundingMode.HALF_UP);
        _precioCaja = _precioCaja.setScale(2, RoundingMode.HALF_UP);
        BigDecimal _precioUnitario = _precioCaja.divide(_unidadesPorCaja, 2, RoundingMode.HALF_UP);

        //buscar cantidad de decimales a usar segun digitos de unidades por caja.  Ej: 24 unidades son dos digitos.
        BigDecimal _unidadesPedidas = unidadesPedidasPorCaja(pCantidadPedida, unidadesPorCaja);

        if (_unidadesPedidas.compareTo(_unidadesPorCaja) >= 0){
            return new BigDecimal(0);
        }
        BigDecimal _totalLineaPorUnidades = _unidadesPedidas.multiply(_precioUnitario).setScale(2, RoundingMode.HALF_UP);
        BigDecimal _totalLineaPorCajas = _cajasPedidas.multiply(_precioCaja).setScale(2, RoundingMode.HALF_UP);
        BigDecimal _totalLineaPedido = _totalLineaPorCajas.add(_totalLineaPorUnidades).setScale(2, RoundingMode.HALF_UP);
        return _totalLineaPedido;
    }

    /*
     * Calcular las unidades pedidas en notacion CAJA.UNIDADES basado en una caja segun formateo numerico de decimales.
     */
    public static BigDecimal unidadesPedidasPorCaja(String pCantidadPedida, long unidadesPorCaja){
        unidadesPorCaja = unidadesPorCaja == 0 ? 1 : unidadesPorCaja;
        //buscar cantidad de decimales a usar segun digitos de unidades por caja.  Ej: 24 unidades son dos digitos.
        long digitos = unidadesPorCaja;
        int ceros = 1;
        while (digitos > 0){
            digitos = digitos/10;
            if (digitos > 0)
                ceros += 1;
        }

        int posicionPuntoDecimal = pCantidadPedida.indexOf(".");
        String digitosString = "";
        BigDecimal _unidadesPedidas = new BigDecimal("0");
        if (posicionPuntoDecimal >= 0){
            digitosString = pCantidadPedida.substring(posicionPuntoDecimal+1);
             //agregar ceros segun cantidad de digitos en unidades por caja.  Ej.  2.05 son dos cajas y cinco unidades en caja 24
            while (digitosString.length() < ceros){
                digitosString = digitosString + "0";
            }
            if (digitosString.length()> ceros){
                return new BigDecimal(0);
            }
            _unidadesPedidas = new BigDecimal(digitosString);
        } else {
            _unidadesPedidas = new BigDecimal("0");
        }
        return _unidadesPedidas;
    }

      public static long unidadesTotalesConvertidas(double cantidad, long unidadesPorCaja){

    	double unidades = NumberUtil.unidadesPedidasPorCaja(""+cantidad, unidadesPorCaja).doubleValue();
        long cant = (long)cantidad;
        long uni = (long) unidades;
		long  total = (cant * unidadesPorCaja) + uni;

		return total;
    }

    public static double unidadesACajaUnidad(long cantidad, long unidadesPorCaja){

    	long caja = (long)cantidad/unidadesPorCaja;
    	long unidades = (long) cantidad % unidadesPorCaja;

    	return numeroCajaUnidades(caja, unidades, unidadesPorCaja);

    }

    public static double numeroCajaUnidades(long caja, long unidades, long unidadesPorCaja){

    	 unidadesPorCaja = unidadesPorCaja == 0 ? 1 : unidadesPorCaja;
    	 long digitos = unidadesPorCaja;
    	 double decimal = unidades;
    	 double total = 0;
         int ceros = 1;
         while (digitos > 0){
             digitos = digitos/10;
             if (digitos > 0)
                 ceros += 1;
         }

         while(ceros > 0){
        	 decimal /= 10;
        	 ceros--;
         }

         total = caja + decimal;

    	return total;
    }
    
    public static BigDecimal unidadesACajaUnidadBD( long cantidad, long unidadesPorCaja ){
        long caja = (long)cantidad/unidadesPorCaja;
        long unidades = (long) cantidad % unidadesPorCaja;

        return numeroCajaUnidadesBD( caja, unidades, unidadesPorCaja );
    }
    
    public static BigDecimal numeroCajaUnidadesBD( long caja, long unidades, long unidadesPorCaja ){
         unidadesPorCaja = unidadesPorCaja == 0 ? 1 : unidadesPorCaja;
    	 long digitos = unidadesPorCaja;
    	 BigDecimal decimal =  new BigDecimal( unidades );
    	 BigDecimal total;
         int ceros = 1;
         while (digitos > 0){
             digitos = digitos/10;
             if (digitos > 0)
                 ceros += 1;
         }

         while(ceros > 0){
             decimal = decimal.divide( BigDecimal.TEN );
             ceros--;
         }
         total = new BigDecimal( caja).add(  decimal );
         return total;

    }

}
