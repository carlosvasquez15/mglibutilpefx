package com.mobilges.lib.util;

import org.jasypt.util.text.BasicTextEncryptor;

/**
 *
 * @author victor
 */
public class EncriptadorJasypt {

    public EncriptadorJasypt(){
    }

    public static String cryptPassword(String password){
        BasicTextEncryptor passwordEncryptor = new BasicTextEncryptor();
        passwordEncryptor.setPassword("MCSi1*=a{F2k!$yP5oMqc/MCs");                     // we HAVE TO set a password
        //passwordEncryptor.setAlgorithm("PBEWithMD5AndTripleDES");    // optionally set the algorithm
        String passwordDigest= passwordEncryptor.encrypt(password);
        return passwordDigest;
    }

    public static String decryptPassword(String password){
        //StandardPBEStringEncryptor passwordEncryptor = new StandardPBEStringEncryptor();
        BasicTextEncryptor passwordEncryptor = new BasicTextEncryptor();
        passwordEncryptor.setPassword("MCSi1*=a{F2k!$yP5oMqc/MCs");                     // we HAVE TO set a password
        //passwordEncryptor.setAlgorithm("PBEWithMD5AndTripleDES");    // optionally set the algorithm
        String passwordDigest= passwordEncryptor.decrypt(password);
        return passwordDigest;
    }

    public static boolean validatePassword(String plainText, String digest){
        String decripted=decryptPassword(digest);
        if(plainText.equals(decripted)){
            return true;
        }else{
            return false;
        }
    }

}
