/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobilges.lib.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author cvasquez
 */
public class XLSUtil {
    public static final String XLSX_ESTENSION = "xlsx";
    public static final String XLS_ESTENSION = "xls";
    
    public static MgTransactionRC uploadFile(UploadedFile file){
        MgTransactionRC  outcome = new MgTransactionRC();
        if( file != null ) {
            try {
                String extension = "";
                int i = file.getFileName().lastIndexOf('.');
                if (i >= 0) {
                    extension = file.getFileName().substring(i+1);
                }
                outcome = convertXLSToList( file.getInputstream(), extension );
                
            } catch ( IOException ex ) {
                //MensajePantalla.error("", "Error al cargar el archivo. ");
                outcome.setSuccessful(false);
            }
        }
        return outcome;
    }
    
    public static MgTransactionRC convertXLSToList(InputStream inputStream , String extension ){
        MgTransactionRC  outcome = new MgTransactionRC();
        List<Object> contenidoList = new ArrayList();
        
        try {
            Workbook workbook;
            Sheet datatypeSheet;
            if(extension.equalsIgnoreCase(XLSX_ESTENSION)){
                workbook = new XSSFWorkbook(inputStream);
                datatypeSheet = (XSSFSheet) workbook.getSheetAt(0);
            }else if(extension.equalsIgnoreCase(XLS_ESTENSION)){
                workbook = new HSSFWorkbook(inputStream); 
                datatypeSheet = (HSSFSheet) workbook.getSheetAt(0);
            }else{
                workbook = new HSSFWorkbook(inputStream);
                datatypeSheet = (HSSFSheet) workbook.getSheetAt(0);
            }
            Iterator<Row> iterator = datatypeSheet.iterator();
            
            //RECORRER FILAS
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();

                //RECORRER COLUMNAS
                List<Object> contenidoColumnaList = new ArrayList();

                for(int i = 0; i < currentRow.getLastCellNum(); i++) {
                    Cell currentCell = currentRow.getCell(i,Row.CREATE_NULL_AS_BLANK);
                    Object contenidoColumna = null;
                    switch( currentCell.getCellTypeEnum() ){
                        case STRING:
                            contenidoColumna = currentCell.getStringCellValue() != null ? currentCell.getStringCellValue() : new String();
                            break;
                        case NUMERIC:
                            contenidoColumna = (Double)currentCell.getNumericCellValue();
                            break;
                        case FORMULA:
                            if( currentCell.getCachedFormulaResultTypeEnum().equals( CellType.STRING ) ){
                                contenidoColumna = currentCell.getStringCellValue() != null ? currentCell.getStringCellValue() : new String();
                            }else{
                                contenidoColumna = (Double)currentCell.getNumericCellValue();
                            }
                            break;
                    }
                    contenidoColumnaList.add( contenidoColumna );
                }
                if(currentRow.getRowNum() > 0 && contieneValor(currentRow)){
                    contenidoList.add(contenidoColumnaList);
                    
                }
            }    
        } 
        catch ( FileNotFoundException ex ) {
            outcome.setObjeto(null);
            outcome.setSuccessful(false);
        } catch ( IOException ex ) {
            outcome.setObjeto(null);
            outcome.setSuccessful(false);
            Logger.getLogger( XLSUtil.class.getName() ).log( Level.SEVERE, null, ex );
        } catch( Exception ex ) {
            outcome.setObjeto(null);
            outcome.setSuccessful(false);
            Logger.getLogger( XLSUtil.class.getName() ).log( Level.SEVERE, null, ex );
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));  
        }
        if(!contenidoList.isEmpty()){
            outcome.setSuccessful(true);
            outcome.setObjeto(contenidoList);
            outcome.setErrorCode("Successful upload.");
        }else{
            outcome.setObjeto(null);
            outcome.setSuccessful(false);
            outcome.setErrorCode("Empty File");
        }
        return outcome;
    }
    
    public static boolean contieneValor(Row row) {
        boolean filaLlena = false;
        for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
        if (StringUtils.isEmpty(String.valueOf(row.getCell(i))) == true || 
            StringUtils.isWhitespace(String.valueOf(row.getCell(i))) == true || 
            StringUtils.isBlank(String.valueOf(row.getCell(i))) == true || 
            String.valueOf(row.getCell(i)).length() == 0 || 
            row.getCell(i) == null) {} 
        else {
            filaLlena = true;
            }
        }
       return filaLlena;
    }
    
    public static boolean validarCantidadDeColumnas(List<Object> datosConvertidosList, int numeroColumnas){
        boolean valido = false;
        List<Object> filasCorrectas = new ArrayList();
        if(datosConvertidosList != null && !datosConvertidosList.isEmpty()){
            for(Object preguntaObject : datosConvertidosList){
                if(preguntaObject != null){
                    List<Object> preguntaObjectColummns = (ArrayList) preguntaObject;
                    if(preguntaObjectColummns.size() >= numeroColumnas){
                        filasCorrectas.add(preguntaObject);
                        
                    }
                }
            }
            if(datosConvertidosList.size() == filasCorrectas.size()){
                valido = true;
            }
        }
        return valido;
    }
}
