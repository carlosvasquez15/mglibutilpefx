package com.mobilges.lib.util;

/**
 * Utilitarios para conversion de datos en query multiple
 * @author victor
 */
public class QueryMultipleUtil {

    public static String setWildCard(String columna) {
        try {
            if (columna == null) {
                return "%";
            } else if (columna.isEmpty()) {
                return "%";
            } else {
                return "%" + columna + "%";
            }
        } catch (Exception ex) {
            return "%";
        }
    }

    public static String setWildCardUpperCase(String columna) {
        try {
            if (columna == null) {
                return "%";
            } else if (columna.isEmpty()) {
                return "%";
            } else {
                return "%" + columna.toUpperCase() + "%";
            }
        } catch (Exception ex) {
            return "%";
        }
    }

    /*
     * Construye query con dedicatoria a Oracle, que no entiende string vacío y lo trata como NULL
     * Primero se construye el Query String, despues se hace set de parametros incluyendo pregunta por "%", despues se llama al query.
     *
     * En columnName se puede enviar UPPER(columnName).  La rutina quita el UPPER al preguntar por ISNULL.
     *
     * Hay que quitar cualquier calificador de columnName en la comparacion ISNULL para evitar error de sql select.
     * Por ejemplo, database Progress no funciona  "Select UPPER(columna) IS NULL"
     *
     * Si parametro 'value' tiene valor, se construye un query sin preguntar por IS NULL
     * 
     */
    public static String genQueryParam(String columnName, String paramName, String value) {
        String parameter;
        String columnNamePlain;
        int firstIndex = columnName.indexOf("(");
        if (firstIndex >= 0) {
            int endIndex = columnName.indexOf(")");
            columnNamePlain = columnName.substring(firstIndex+1, endIndex);
        } else{
            columnNamePlain = columnName;
        }

        if (value == null || value.length() <= 0) {
            parameter = "   AND (" + columnName + " like :" + paramName + " OR " + columnNamePlain + " IS NULL)";
        } else {
            parameter = "   AND " + columnName + " like :" + paramName;
        }
        return parameter;
    }
}
