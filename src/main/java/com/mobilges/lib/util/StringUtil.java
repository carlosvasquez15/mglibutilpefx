package com.mobilges.lib.util;

import java.util.Set;
import java.util.StringTokenizer;

/**
 * Utilidades para manejo de strings
 * @author victor
 */
public class StringUtil {
    //Reemplaza una cadena de texto dentro de el string str por el valor replace

    public static String replaceParamStr(String str, String pattern, String replace) {
        int s = 0;
        int e = 0;
        StringBuffer result = new StringBuffer();
        while ((e = str.indexOf(pattern, s)) >= 0) {
            result.append(str.substring(s, e));
            result.append(replace);
            s = e + pattern.length();
        }
        result.append(str.substring(s));
        return result.toString();
    }

    /* remove blanks between words*/
    public static String removeBlanks(String stSource) {
        String t = "";
        if (stSource != null) {
            StringTokenizer st = new StringTokenizer(stSource, " ", false);
            while (st.hasMoreElements()) {
                t += st.nextElement();
            }
        }
        return t;
    }

    /* used to trim properties to max database length and avoid update errors  */
    public static String trimToMaxLength(String st, Integer maxLength){
        String newSt = "";
        if (st != null){
            newSt = st.length() <= maxLength ? st : st.substring(0, maxLength);
        }
        return newSt;
    }
    
    public static String setToCommaSeparatedString( String prefix, Set<String> set ){
          boolean primero = true;
        String commaString = "";
        for ( String valorTipologia : set ) {
            if( primero ) {
                commaString = prefix.concat( "." ).concat( valorTipologia );
                primero = false;
            } else {
                commaString += "," + prefix.concat( "." ).concat( valorTipologia );
            }
        }
        return commaString;
    }
    
    public static String setToCommaSeparatedString( Set<String> set ) {
        boolean primero = true;
        String commaString = "";
        for ( String valorTipologia : set ) {
            if( primero ) {
                commaString = valorTipologia;
                primero = false;
            } else {
                commaString += "," + valorTipologia;
            }
        }
        return commaString;
    }
    
    public static String setToCommaSeparatedStringLong( Set<Long> set ) {
        boolean primero = true;
        String commaString = "";
        for ( Long valorTipologia : set ) {
            if( primero ) {
                commaString = valorTipologia.toString();
                primero = false;
            } else {
                commaString += "," + valorTipologia.toString();
            }
        }
        return commaString;
    }
    
    public static String setToCommaSeparatedStringEscaped( Set<String> set ) {
        boolean primero = true;
        String commaString = "";
        for ( String valorTipologia : set ) {
            if( primero ) {
                commaString = "'" + valorTipologia + "'";
                primero = false;
            } else {
                commaString += "," + "'" + valorTipologia + "'";
            }
        }
        return commaString;
    }
}
