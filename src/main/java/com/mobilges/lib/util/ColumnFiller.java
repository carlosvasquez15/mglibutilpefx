package com.mobilges.lib.util;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase diseñada para llenar un objeto a partir de un listado de columnas y un array de valores Object[]
 * @author victor
 */
public class ColumnFiller {
    public static Object llenarCampos( Object pObjeto, String[] pColumnas, Object[] pValores ){
        String[] columnas=pColumnas;
        int idx=0;
        for(String columna:columnas){
            Method metodoGet = null;
            Method metodoSet = null;
            try {
                String metodoGetString = "get" + columna.substring(0, 1).toUpperCase() + columna.substring(1);
                metodoGet = pObjeto.getClass().getMethod(metodoGetString, new Class[0]);
            } catch (NoSuchMethodException noSuchMethodException) {
                try {
                    String metodoGetString = "is" + columna.substring(0, 1).toUpperCase() + columna.substring(1);
                    metodoGet = pObjeto.getClass().getMethod(metodoGetString, new Class[0]);
                } catch (NoSuchMethodException noSuchMethodException1) {
                } catch (SecurityException securityException) {
                }
            } catch (SecurityException securityException) {
            }

            try {
                String metodoSetString = "set" + columna.substring(0, 1).toUpperCase() + columna.substring(1);
                metodoSet = pObjeto.getClass().getMethod(metodoSetString, new Class[]{metodoGet.getReturnType()});
            } catch (NoSuchMethodException noSuchMethodException) {
            } catch (SecurityException securityException) {
            }
            //Si existe metodo set
            if(metodoSet!=null){
                try {
                    metodoSet.invoke(pObjeto, pValores[idx]);
                } catch (IllegalAccessException illegalAccessException) {
                } catch (IllegalArgumentException illegalArgumentException) {
                } catch (InvocationTargetException invocationTargetException) {
                }
            }
            idx++;
        }
        return pObjeto;
    }
    
    //Version ALPHA de método que soporta valores compuestos
    public static Object llenarCamposComp( Object pObjeto, String[] pColumnas, Object[] pValores ){
        String[] columnas=pColumnas;
        int idx=0;
        for(String columna:columnas){
            Method metodoGet = null;
            Method metodoSet = null;
            Method metodoGetH = null;
            Method metodoSetH = null;
            String[] subcolumnas = columna.split( "\\." );
            boolean compuesto = false;
            compuesto = subcolumnas.length > 1;
            String metodoGetString = "";
            String metodoGetStringH = "";
            try {
              
                if( !compuesto ){
                      metodoGetString = "get" + columna.substring(0, 1).toUpperCase() + columna.substring(1);
                     metodoGet = pObjeto.getClass().getMethod(metodoGetString, new Class[0]);

                } else {
                    metodoGetString = "get" + subcolumnas[0].substring(0, 1).toUpperCase() + subcolumnas[0].substring(1);
                    metodoGet = pObjeto.getClass().getMethod(metodoGetString, new Class[0]);
                    metodoGetStringH = "get" + subcolumnas[1].substring(0, 1).toUpperCase() + subcolumnas[1].substring(1);
                    metodoGetH = pObjeto.getClass().getMethod(metodoGetString, new Class[0]).getReturnType().getMethod( metodoGetStringH, new Class[ 0 ] );
                }
            } catch (NoSuchMethodException noSuchMethodException) {
            } catch (SecurityException securityException) {
            }
            try {
                if( !compuesto ){
                    String metodoSetString = "set" + columna.substring(0, 1).toUpperCase() + columna.substring(1);
                    metodoSet = pObjeto.getClass().getMethod(metodoSetString, new Class[]{metodoGet.getReturnType()});
                }
                else {
                    String metodoSetString = "set" + subcolumnas[0].substring(0, 1).toUpperCase() + subcolumnas[0].substring(1);
                    metodoSet = pObjeto.getClass().getMethod(metodoSetString, new Class[]{metodoGet.getReturnType()});
                    String metodoSetStringH = "set" + subcolumnas[1].substring(0, 1).toUpperCase() + subcolumnas[1].substring(1);
                    metodoSetH = pObjeto.getClass().getMethod( metodoGetString, new Class[0] ).getReturnType().getMethod(  metodoSetStringH, new Class[]{ metodoGetH.getReturnType()} );
//                    metodoSetH = pObjeto.getClass().getMethod(metodoSetString, new Class[]{metodoGet.getReturnType()}).getReturnType().getMethod( metodoSetStringH, new Class[]{ metodoGetH.getReturnType() });
                }
            } catch (NoSuchMethodException noSuchMethodException) {
            } catch (SecurityException securityException) {
            }
            //Si existe metodo set
            if(metodoSet!=null){
                try {
                    if( !compuesto ){
                        metodoSet.invoke(pObjeto, pValores[idx]);
                    } else {
                        Object obj = metodoGet.getReturnType().newInstance();
                        obj = llenarCampos( obj, subcolumnas[1], pValores[ idx ] );
                        //metodoSetH.invoke( obj, pValores[ idx ] );
                        metodoSet.invoke(pObjeto, obj );
                    }
                    
                } catch (IllegalAccessException illegalAccessException) {
                } catch (IllegalArgumentException illegalArgumentException) {
                } catch (InvocationTargetException invocationTargetException) {
                } catch ( InstantiationException ex ) {
                    Logger.getLogger( ColumnFiller.class.getName() ).log( Level.SEVERE, null, ex );
                }
            }
            
            
            
            idx++;
        }
        return pObjeto;
    }
  
    /**
     * Metodo que llena una sola columna, utilizar cuando las consultas son sobre una sola columna
     */
    public static Object llenarCampos(Object pObjeto, String pColumna, Object pValores) {
        Method metodoGet = null;
        Method metodoSet = null;
        try {
            String metodoGetString = "get" + pColumna.substring(0, 1).toUpperCase() + pColumna.substring(1);
            metodoGet = pObjeto.getClass().getMethod(metodoGetString, new Class[0]);
        } catch (NoSuchMethodException noSuchMethodException) {
            try {
                String metodoGetString = "is" + pColumna.substring(0, 1).toUpperCase() + pColumna.substring(1);
                metodoGet = pObjeto.getClass().getMethod(metodoGetString, new Class[0]);
            } catch (NoSuchMethodException noSuchMethodException1) {
            } catch (SecurityException securityException) {
            }
        } catch (SecurityException securityException) {
        }

        try {
            String metodoSetString = "set" + pColumna.substring(0, 1).toUpperCase() + pColumna.substring(1);
            metodoSet = pObjeto.getClass().getMethod(metodoSetString, new Class[]{metodoGet.getReturnType()});
        } catch (NoSuchMethodException noSuchMethodException) {
        } catch (SecurityException securityException) {
        }
        //Si existe metodo set
        if (metodoSet != null) {
            try {
                metodoSet.invoke(pObjeto, pValores);
            } catch (IllegalAccessException illegalAccessException) {
            } catch (IllegalArgumentException illegalArgumentException) {
            } catch (InvocationTargetException invocationTargetException) {
            }
        }
        return pObjeto;
    }
    
    private static final Set<Class> WRAPPER_TYPES = new HashSet( Arrays.asList(
            Boolean.class, Character.class, Byte.class, Short.class, Integer.class, Long.class, Float.class, Double.class, Void.class ) );

    public static boolean isWrapperType( Class clazz ) {
        return WRAPPER_TYPES.contains( clazz );
    }
    
}
