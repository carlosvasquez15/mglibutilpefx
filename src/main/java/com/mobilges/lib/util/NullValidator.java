package com.mobilges.lib.util;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Clase diseñada para eliminar campos nulos
 * @author victor
 */
public class NullValidator {

    public static Object validarCampos(Object pObjeto) {
        Object objeto = pObjeto;
        Method metodos[] = pObjeto.getClass().getMethods();
        for (int i = 0; i < metodos.length; i++) {
            Method metodo = metodos[i];
            //Si es un metodo get o is lo utilizo con su equivalente set
            if ((metodo.getName().substring(0, 3).equalsIgnoreCase("get") || metodo.getName().substring(0, 2).equalsIgnoreCase("is")) && !metodo.getName().equals("getClass")) {
                String methodNameSet = "";
                if (metodo.getName().substring(0, 3).equalsIgnoreCase("get")) {
                    methodNameSet = metodo.getName().replaceFirst("get", "set");
                } else {
                    methodNameSet = methodNameSet.replaceFirst("is", "set");
                }
                try {
                    Method metodoSet = pObjeto.getClass().getMethod(methodNameSet, metodo.getReturnType());
                    if (metodoSet != null) {
                        //Datos numericos
                        //Si es byte
                        if (metodo.getReturnType().equals(java.lang.Byte.class)) {
                            Byte valor = (Byte) metodo.invoke(pObjeto, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObjeto, 0);
                            }
                        }
                        //Si es bigDecimal
                        if (metodo.getReturnType().equals(java.math.BigDecimal.class)) {
                            BigDecimal valor = (BigDecimal) metodo.invoke(pObjeto, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObjeto, new BigDecimal(0));
                            }
                        }
                        // Si es Double
                        if (metodo.getReturnType().equals(java.lang.Double.class)) {
                            Double valor = (Double) metodo.invoke(pObjeto, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObjeto, new Double(0));
                            }
                        }
                        //Si es un string
                        if (metodo.getReturnType().equals(java.lang.String.class)) {
                            String valor = (String) metodo.invoke(pObjeto, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObjeto, "");
                            }
                        }
                        //Si es una lista
                        if (metodo.getReturnType().equals(java.util.List.class)) {
                            List objetosList = (List) metodo.invoke(pObjeto, new Object[0]);
                            for (Object objetoFromList : objetosList) {
                                NullValidator.validarCampos(objetoFromList);
                            }
                        }
                        //Si es date
                        if (metodo.getReturnType().equals(java.util.Date.class)) {
                            Date valor = (Date) metodo.invoke(pObjeto, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObjeto, new Date());
                            }
                        }
                        //Si es long
                        if (metodo.getReturnType().equals(java.lang.Long.class)) {
                            Long valor = (Long) metodo.invoke(pObjeto, new Object[0]);
                            if (valor == null) {
                                metodoSet.invoke(pObjeto, new Long(0));
                            }
                        }
                        //Si es primitivo
                        if (metodo.getReturnType().isPrimitive()) {
                            //los primitivos no permiten null
                        }
                    }
                } catch (Exception e) {
                }
            //System.out.println(metodo.getName() + " " + methodNameSet);
            }
        }
        return objeto;
    }

    /**
     * Excluye los objetos dentro de List para acelerar el rendimiento
     */
    public static Object validarCamposNoList(Object pObjeto) {
        Object objeto = pObjeto;

        Method metodos[] = pObjeto.getClass().getMethods();
        List<Method> metodosList = Arrays.asList(metodos);
        Iterator metodosIterator = metodosList.iterator();
        int i=0;
        while (metodosIterator.hasNext()) {
            System.out.println(i++);
            Method metodo = (Method) metodosIterator.next();
            System.out.println(metodo.getName());
            try {
                //Si es un metodo get o is lo utilizo con su equivalente set
                if (metodo.getName().substring(0, 3).equalsIgnoreCase("set") || metodo.getName().equals("getClass") || metodo.getReturnType().isPrimitive() || metodo.invoke(pObjeto, new Object[0]) != null) {
                    continue;
                }
                String methodNameSet = "";
                if (metodo.getName().substring(0, 3).equalsIgnoreCase("get")) {
                    methodNameSet = metodo.getName().replaceFirst("get", "set");
                } else {
                    methodNameSet = methodNameSet.replaceFirst("is", "set");
                }
                System.out.println(methodNameSet);
                Method metodoSet = pObjeto.getClass().getMethod(methodNameSet, metodo.getReturnType());
                if (metodoSet != null) {
                    //Datos numericos
                    //Si es byte

                    if (metodo.getReturnType().equals(java.lang.Byte.class)) {
                        metodoSet.invoke(pObjeto, 0);
                    } else if (metodo.getReturnType().equals(java.math.BigDecimal.class)) {
                        metodoSet.invoke(pObjeto, new BigDecimal(0));
                    } else if (metodo.getReturnType().equals(java.lang.Double.class)) {
                        metodoSet.invoke(pObjeto, new Double(0));
                    } else if (metodo.getReturnType().equals(java.lang.String.class)) { //Si es un string
                        metodoSet.invoke(pObjeto, "");
                    } else if (metodo.getReturnType().equals(java.util.Date.class)) {
                        metodoSet.invoke(pObjeto, new Date());
                    }

                }
            } catch (Exception e) {
            }
        //System.out.println(metodo.getName() + " " + methodNameSet);
        }
        return objeto;
    }
}
