package com.mobilges.lib.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Copies field value from sourceObject to destinationObject
 * Used specially to build objects that will be transferred via WebServices
 * The method cloneClass will operate only on fields that are declared on
 * both objects.
 * sourceObject is the original Object
 * destinationObject is the destination Object
 *
 * Name and Type must match
 * 
 * @author as
 */
public class CloneServerToMobilClass {

    public CloneServerToMobilClass() {
        
    }

    public static Object cloneClass(Object sourceObject, Object destinationObject)
             {
        try {
            Field movilFields[] = destinationObject.getClass().getDeclaredFields();
            Field serverFields[] = sourceObject.getClass().getDeclaredFields();
            int len = movilFields.length;
            for (int i = 0; i < len; i++) {
                Field movilField = movilFields[i];
                for (Field serverField : serverFields) {
                    //if (movilField.getName().equals(serverField.getName())) {
                    if (movilField.getName().equals(serverField.getName()) && (movilField.getGenericType().equals(serverField.getGenericType())) ) {
                        String methodNameGet = (new StringBuilder()).append("get").append(movilField.getName().substring(0, 1).toUpperCase()).append(movilField.getName().substring(1)).toString();
                        String methodNameSet = (new StringBuilder()).append("set").append(movilField.getName().substring(0, 1).toUpperCase()).append(movilField.getName().substring(1)).toString();
                        Method methodGet = sourceObject.getClass().getMethod(methodNameGet, new Class[0]);
                        Method methodSet = destinationObject.getClass().getMethod(methodNameSet, new Class[]{methodGet.getReturnType()});
                        methodSet.invoke(destinationObject, new Object[]{methodGet.invoke(sourceObject, new Object[0])});
                    }
                }
            }

        } catch (Exception ex) {
            return null;
        }
        return destinationObject;
    }
}
