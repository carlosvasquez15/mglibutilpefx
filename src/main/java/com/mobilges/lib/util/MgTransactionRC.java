package com.mobilges.lib.util;

import java.io.Serializable;

/**
 *
 * @author as
 *
 * Transaction Return Code
 * 
 * @param dbErrorCode is database error number.
 * @param dbErrorMessage is message associated to database error message or process error message.
 * @param errorCode is internal application error number, asociated with resource bundle.
 * @param object that was processed if persist operation was done - returns null if error.
 * @param successful indicates if the transaccion has succeed or not.
 */

public class MgTransactionRC implements Serializable {

    // these codes are used within EJB within persist transaction.
    public static final int toPersist = 1;
    public static final int toMerge = 2;
    public static final int toRemove = 3;

    // these codes have their corresponding Resource Bundle text in Mg_Textos_es and Mg_Textos_en
    public static final String msgPersistSuccess = "msgPersistSuccess";
    public static final String msgRemoveSuccess = "msgRemoveSuccess";
    public static final String msgErrorTransaction = "msgErrorTransaction";
    public static final String msgObjectNoExist = "msgObjectNoExist";
    public static final String msgObjectExists = "msgObjectExists";
    public static final String msgNoOperation = "msgNoOperation";
    public static final String msgTrAlreadyExists = "msgTrAlreadyExists";


    private String dbErrorCode;
    private String dbErrorMessage;
    private String errorCode;
    private Object objeto;
    private boolean successful;

    public MgTransactionRC(){
        dbErrorCode = "";
        dbErrorMessage = "";
        errorCode = "";
        objeto = null;
        successful = false;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Object getObjeto() {
        return objeto;
    }

    public void setObjeto(Object objeto) {
        this.objeto = objeto;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public String getDbErrorCode() {
        return dbErrorCode;
    }

    public void setDbErrorCode(String dbErrorCode) {
        this.dbErrorCode = dbErrorCode;
    }

    public String getDbErrorMessage() {
        return dbErrorMessage;
    }

    public void setDbErrorMessage(String dbErrorMessage) {
        this.dbErrorMessage = dbErrorMessage;
    }

}
