
package com.mobilges.lib.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Para recuperar el mensaje de error que corresponde
 *
 * @author as
 */

public class MessageUtil {

    public static String getMessageByTransactionReturnCode(String errorCode, String idioma){
        Locale localizacion;
        if (idioma.equals("en")) {
            localizacion = new Locale("en", "US", "");
        } else {
            localizacion = new Locale("es", "GT", "");
        }
        ResourceBundle rb = ResourceBundle.getBundle("com.mobilges.server.lib.model.base.MgTextosMsg", localizacion);
        String mensaje = "";
        try{
            mensaje = rb.getString(errorCode);
        } catch (Exception ex){
            mensaje = "Mensaje No Disponible - Message Not Available";
        }
        return mensaje;
    }

}
