
package com.mobilges.lib.util.web;

/**
 *
 * @author as
 *
 * Object to set data on a screen, it is send to PickListScreen
 *
 * Executing the callback, the object can be cast to PickListElements to use
 * codigo and descripcion, or cast to the object required like inventario,
 * cliente, etc.
 */

public interface PickListSetData {

    public void setData(Object selectedObject );

}
