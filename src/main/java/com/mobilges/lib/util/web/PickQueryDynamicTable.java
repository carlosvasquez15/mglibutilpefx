
package com.mobilges.lib.util.web;

import java.util.ArrayList;

/**
 *
 * @author as
 *
 * To set data on web applications pick screen
 */
public interface PickQueryDynamicTable {

        ArrayList<PickListElements> getDynamicTable( String pCodigoSearch, String pNombreSearch);

}
