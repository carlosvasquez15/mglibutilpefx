
package com.mobilges.lib.util.web;

/**
 *
 * @author as
 *
 * Interface used to display records to pick
 * Records come from different sources
 *
 * getNombreForPickList returns the generic name or description of the object
 *
 * getCodigoForPickList returns the codigo or ID of the object
 *
 * Selected is used for list-display controls
 *
 */
public interface PickListElements {

    String getNombreForPickList();
    String getCodigoForPickList();
    boolean isSelected();
    void setSelected(boolean selected);
    

}
